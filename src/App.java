import java.util.Date;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer("Duong");
        Customer customer2 = new Customer("Linh", true, "VIP");

        System.out.println("customer1 :  " + customer1);
        System.out.println("customer2 :  " + customer2);

        Visit visit1 = new Visit(customer1, new Date(), 20000, 12000);
        Visit visit2 = new Visit(customer2, new Date(), 40000, 22000);

        System.out.println("visit1 :  " + visit1);
        System.out.println("visit2 :  " + visit2);

        visit1.getTotalExpense();
        visit2.getTotalExpense();
        System.out.println(visit1);
        System.out.println(visit2);
    }
}
