import java.util.Date;

public class Visit {
    private Customer customer ;
    private Date date ;
    private double ServiceExpense ;
    private double productExpense ;

   

    public Visit(Customer customer, Date date, double serviceExpense, double productExpense) {
        this.customer = customer;
        this.date = date;
        ServiceExpense = serviceExpense;
        this.productExpense = productExpense;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getServiceExpense() {
        return ServiceExpense;
    }

    public void setServiceExpense(double serviceExpense) {
        ServiceExpense = serviceExpense;
    }

    public double getProductExpense() {
        return productExpense;
    }

    public void setProductExpense(double productExpense) {
        this.productExpense = productExpense;
    }

    public double getTotalExpense(){
        double TotalExpense = this.productExpense + this.ServiceExpense ;
         return TotalExpense ;
    }
    
    @Override
    public String toString() {
        return "Visit [Customer [name=" + customer.getName() + ", member=" + customer.isMember() + ", memberType=" + customer.getMemberType() + "], date=" + date + ", ServiceExpense=" + ServiceExpense
                + ", productExpense=" + productExpense + ", TotalExpense=" + this.getTotalExpense() +"]";
    }

    
}
